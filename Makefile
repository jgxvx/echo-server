REGISTRY=registry.gitlab.com
PROJECT=jgxvx/echo-server
IMAGE=$(REGISTRY)/$(PROJECT)
RUST_LOG=debug
IP=0.0.0.0
PORT=8000
VERSION=latest

.PHONY: docker-build
docker-build:
	docker build --build-arg RUST_LOG=$(RUST_LOG) --build-arg IP=$(IP) --build-arg PORT=$(PORT) -t $(IMAGE):$(VERSION) .

.PHONY: docker-run
docker-run:
	docker run --init -p "$(PORT):$(PORT)" --rm $(IMAGE):$(VERSION)

.PHONY: docker-push
docker-push:
	docker push $(IMAGE):$(VERSION)
