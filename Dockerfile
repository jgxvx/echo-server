FROM rust:1.63 AS builder
WORKDIR /app
COPY . /app
RUN cargo build --release

FROM gcr.io/distroless/cc
COPY --from=builder /app/target/release/echo-server /

ARG RUST_LOG
ARG IP
ARG PORT

ENV RUST_LOG=$RUST_LOG
ENV IP=$IP
ENV PORT=$PORT

CMD ["./echo-server"]
