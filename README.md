# jgxvx/echo-server

A simple HTTP echo server to be deployed to Kubernetes.

## Usage

### HTTP Endpoints

```shell
curl http://localhost:8000/foo
curl http://localhost:8000/bar
```

## Deployment

```shell
helm upgrade echo-server ./devops/helm/echo-server \
  --namespace=the-namespace \
  --install \
  --atomic \
  --timeout="60s" \
  --set-string appVersion="1.0.0" \
  --set-string ingress.hosts.default.name="echo-server.local"
```
This will create a deployment, a service and an ingress rule that will route all requests to the echo-server pod.
